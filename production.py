# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pool import Pool

__all__ = ['Production']


class Production(metaclass=PoolMeta):
    __name__ = 'production'

    parent = fields.Many2One('production', 'Parent',
        ondelete='CASCADE')
    childs = fields.One2Many('production', 'parent', 'Children')

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls._error_messages.update({
            'producible_no_bom': ('The product "%s" is producible but '
                'has no BOM.'),
            })

    def is_child_producible(self, product):
        return product.producible if product else False

    def create_production_value(self, input, quantity=None):
        if not input.product.boms:
            self.raise_user_error('producible_no_bom',
                (input.product.rec_name,))

        production_value = {
            'product': input.product,
            'warehouse': self.warehouse.id,
            'location': self.location.id,
            'uom': input.uom,
            'quantity': quantity or input.quantity,
            'bom': input.product.boms[0].bom,
            'effective_start_date': self.effective_start_date,
            'effective_date': self.effective_date
        }
        return production_value

    def produce_oncascade(self):
        Production = Pool().get('production')

        to_create = []
        for _input in self.inputs:
            if self.is_child_producible(_input.product):
                to_create.append(self.create_production_value(_input))
        if not to_create:
            return

        productions = Production.create(to_create)
        for new_production in productions:
            new_production.parent = self
            new_production.on_change_bom()
            new_production.save()
            new_production.produce_oncascade()
        Production.wait(productions)
        Production.assign(productions)

    @classmethod
    def draft(cls, productions):
        super(Production, cls).draft(productions)
        for production in productions:
            cls.draft(production.childs)

    @classmethod
    def wait(cls, productions):
        super(Production, cls).wait(productions)
        for production in productions:
            cls.wait(production.childs)

    @classmethod
    def cancel(cls, productions):
        super(Production, cls).cancel(productions)
        for production in productions:
            cls.cancel(production.childs)

    @classmethod
    def assign(cls, productions):
        super(Production, cls).assign(productions)
        for production in productions:
            production.produce_oncascade()

    @classmethod
    def run(cls, productions):
        super(Production, cls).run(productions)
        for production in productions:
            cls.run(production.childs)

    @classmethod
    def done(cls, productions):
        super(Production, cls).done(productions)
        for production in productions:
            cls.done(production.childs)
