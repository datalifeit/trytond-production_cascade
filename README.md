datalife_production_cascade
===========================

The production_cascade module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_cascade/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_cascade)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
