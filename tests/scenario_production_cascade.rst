===================
Production Scenario
===================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.production.production import BOM_CHANGES
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)
    >>> before_yesterday = yesterday - relativedelta(days=1)

Install production Module::

    >>> config = activate_modules('production_cascade')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.producible = True
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal(30)
    >>> template.save()
    >>> product.template = template
    >>> product.cost_price = Decimal(20)
    >>> product.save()

Create Components::

    >>> component1 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'component 1'
    >>> template1.default_uom = unit
    >>> template1.producible =True
    >>> template1.type = 'goods'
    >>> template1.list_price = Decimal(5)
    >>> template1.save()
    >>> component1.template = template1
    >>> component1.cost_price = Decimal(1)
    >>> component1.save()

    >>> component2 = Product()
    >>> template2 = ProductTemplate()
    >>> template2.producible = False
    >>> template2.name = 'component 2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal(7)
    >>> template2.save()
    >>> component2.template = template2
    >>> component2.cost_price = Decimal(5)
    >>> component2.save()

Create Sub Component1::

    >>> subcomponent1 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'subcomponent 1'
    >>> template1.default_uom = unit
    >>> template1.producible = False
    >>> template1.type = 'goods'
    >>> template1.list_price = Decimal(5)
    >>> template1.save()
    >>> subcomponent1.template = template1

    >>> subcomponent1.save()

    >>> meter, = ProductUom.find([('name', '=', 'Meter')])
    >>> centimeter, = ProductUom.find([('name', '=', 'centimeter')])


Create Bill of Material of Component 1::

    >>> BOM = Model.get('production.bom')
    >>> BOMInput = Model.get('production.bom.input')
    >>> BOMOutput = Model.get('production.bom.output')
    >>> component1_bom = BOM(name='product')
    >>> input1 = BOMInput()
    >>> component1_bom.inputs.append(input1)
    >>> input1.product = subcomponent1
    >>> input1.quantity = 5

    >>> output = BOMOutput()
    >>> component1_bom.outputs.append(output)
    >>> output.product = component1
    >>> output.quantity = 1
    >>> component1_bom.save()

    >>> ProductBom = Model.get('product.product-production.bom')
    >>> component1.boms.append(ProductBom(bom=component1_bom))
    >>> component1.save()

Create Bill of Material::

    >>> BOM = Model.get('production.bom')
    >>> BOMInput = Model.get('production.bom.input')
    >>> BOMOutput = Model.get('production.bom.output')
    >>> bom = BOM(name='product')
    >>> input1 = BOMInput()
    >>> bom.inputs.append(input1)
    >>> input1.product = component1
    >>> input1.quantity = 5
    >>> input2 = BOMInput()
    >>> bom.inputs.append(input2)
    >>> input2.product = component2
    >>> input2.quantity = 2
    >>> output = BOMOutput()
    >>> bom.outputs.append(output)
    >>> output.product = product
    >>> output.quantity = 1
    >>> bom.save()

    >>> ProductBom = Model.get('product.product-production.bom')
    >>> product.boms.append(ProductBom(bom=bom))
    >>> product.save()

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> InventoryLine = Model.get('stock.inventory.line')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line1 = InventoryLine()
    >>> inventory.lines.append(inventory_line1)
    >>> inventory_line1.product = component1
    >>> inventory_line1.quantity = 20
    >>> inventory_line2 = InventoryLine()
    >>> inventory.lines.append(inventory_line2)
    >>> inventory_line2.product = component2
    >>> inventory_line2.quantity = 6
    >>> inventory.click('confirm')

Make a production::

    >>> Production = Model.get('production')
    >>> production = Production()
    >>> production.product = product
    >>> production.bom = bom
    >>> production.quantity = 2
    >>> production.effective_date = yesterday
    >>> production.effective_start_date = before_yesterday
    >>> production.save()
    >>> production.click('wait')

Component 1 is the only one producible so it is created::

    >>> production.click('assign_try')
    True
    >>> productions = Production.find([], order=[('number', 'ASC')])
    >>> [p.product.name for p in productions]
    ['product', 'component 1']

Check transitions::

    >>> production.click('run')
    >>> productions = Production.find([])
    >>> list(set([p.state for p in productions]))
    ['running']
    >>> production.click('done')
    >>> productions = Production.find([])
    >>> list(set([p.state for p in productions]))
    ['done']

Make another production::

    >>> Production = Model.get('production')
    >>> production = Production()
    >>> production.product = product
    >>> production.bom = bom
    >>> production.quantity = 2
    >>> production.effective_date = yesterday
    >>> production.effective_start_date = before_yesterday
    >>> production.save()
    >>> production.click('wait')

Add subcomponent 2 to component 1::

    >>> subcomponent2 = Product()
    >>> template2 = ProductTemplate()
    >>> template2.producible = True
    >>> template2.name = 'subcomponent 2'
    >>> template2.default_uom = meter
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal(7)
    >>> template2.save()
    >>> subcomponent2.template = template2
    >>> subcomponent2.cost_price = Decimal(5)
    >>> subcomponent2.save()

    >>> input2 = BOMInput()
    >>> component1_bom.inputs.append(input2)
    >>> input2.product = subcomponent2
    >>> input2.quantity = 150
    >>> input2.uom = centimeter
    >>> component1_bom.save()

Error is raised as Subcomponent 2 does not have BOM::

    >>> production.click('assign_force') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('The product "subcomponent 2" is producible but has no BOM.'))